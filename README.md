# Alefair.Libraries.ScreenShoter

*The library takes a screenshot of the Uielement along with the bounding box.*

The library is created in UiPath Studio. 
> Designed for UiPath only .

**Dependency:**
-----------
UiPath.System.Activities >= 18.4.1<br>
UiPath.UIAutomation.Activities >= 18.4.2


**Input Arguments:**
----------------
*FileName* - Full Path to File. C:\Temp\Temp.jpg<br>
*MyElement* - The Element to display in the screenshot with the frame<br>
*ParentElement* - Parent element background (or default white background)<br>
*RectWidth* - Frame size in weight (by default, the size of the parent element or the main element is taken)<br>
*RectHeight* - Frame size in height (by default, the size of the parent element or the main element is taken)<br>
*BorderColor* - Color of Border<br>
*BorderSize* - Size of Border<br>